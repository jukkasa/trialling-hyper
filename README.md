# trialling-hyper

Another simple Rust webservice to experiment with things like Hyper, Docker builds, Kubernetes, CI/CD, etc.

## Status

- Updated to Hyper 1.0
- Dockerfiles / cross compilation suboptimal
