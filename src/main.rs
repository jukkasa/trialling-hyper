
use hyper::body::{Body, Bytes};
use hyper::server::conn::http1;
use hyper::service::service_fn;
use hyper::{body::Incoming as IncomingBody, Method,  Request, Response, StatusCode, header};
use hyper_util::rt::TokioIo;
use tokio::net::TcpListener;
use http_body_util::{BodyExt, Full};
use std::net::SocketAddr;

const VERSION: &str = env!("CARGO_PKG_VERSION");

static HTMLPAGE: &[u8] = b"<a href=\"./joke\">Fetch a joke</a>";
static NOTFOUND: &[u8] = b"Trialling Hyper - Page Not Found";

type GenericError = Box<dyn std::error::Error + Send + Sync>;
type Result<T> = std::result::Result<T, GenericError>;
type BoxBody = http_body_util::combinators::BoxBody<Bytes, hyper::Error>;

async fn use_reqwest(_req: Request<IncomingBody>) -> Result<Response<BoxBody>> {
    let request = reqwest::get(
        "https://v2.jokeapi.dev/joke/Any?blacklistFlags=racist,sexist&format=txt&type=single",
    )
    .await;

    let body = match request {
        Ok(request) => {
            let text = request.text().await;
            text.unwrap_or("????".to_string())
        }
        Err(err) => err.to_string(),
    };

    println!("body = {}", body);

    let message = format!(
        "<html><body><h1>Joke from jokeapi.dev</h1><pre>{}</pre></body></html>",
        body
    );

    let response = Response::builder()
        .status(StatusCode::OK)
        .header(header::CONTENT_TYPE, "text/html; charset=utf-8")
        .body(full(message))?;
    Ok(response)
}

async fn index(req: Request<IncomingBody>) -> Result<Response<BoxBody>> {
    let hn = hostname::get()
        .unwrap_or_default()
        .into_string()
        .unwrap_or_default();
    let mut env = "Env:<pre>".to_string();
    for (key, value) in std::env::vars() {
        env.push_str(&format!("    {}: {}\n", key, value));
    }
    env.push_str("</pre><br>");

    let mut hdrs = "Headers:<pre>".to_string();
    for (key, value) in req.headers().iter() {
        hdrs.push_str(&format!(
            "    {}: {}\n",
            key,
            value.to_str().unwrap_or("WHAT! Some binary data???")
        ));
    }
    hdrs.push_str("</pre><br>");

    let message = format!(
        "<html><body><h1>Hyper server v{}</h1>Uri: {}<br>Hostname: {}<br>{}{}</body></html>",
        VERSION,
        req.uri(),
        hn,
        env,
        hdrs
    );

    let response = Response::builder()
        .status(StatusCode::OK)
        .header(header::CONTENT_TYPE, "text/html; charset=utf-8")
        .body(full(message))?;
    Ok(response)
}

async fn post_echo(req: Request<IncomingBody>) -> Result<Response<BoxBody>> {

    let uri = req.uri().clone();

    // To protect our server, reject requests with bodies larger than
    // 64kiB of data.
    let max = req.body().size_hint().upper().unwrap_or(u64::MAX);
    if max > 1024 * 64 {
        let mut resp = Response::new(full("Body too big"));
        *resp.status_mut() = hyper::StatusCode::PAYLOAD_TOO_LARGE;
        return Ok(resp);
    }

    let whole_body = req.collect().await?.to_bytes();
    let incoming_text = String::from_utf8_lossy(&whole_body);

    let body_text = format!(
        "You posted to uri: {}\nYour body:\n{}\n",
        uri, incoming_text);

    let response = Response::builder()
        .status(StatusCode::OK)
        .header(header::CONTENT_TYPE, "text/plain; charset=utf-8")
        .body(full(body_text))?;
    Ok(response)
}

async fn routes(req: Request<IncomingBody>) -> Result<Response<BoxBody>> {
    println!("Request to: {}", req.uri().path());

    if req.method() == Method::GET
        && (req.uri().path().ends_with('/') || req.uri().path().ends_with("/index.html"))
    {
        index(req).await
    } else if req.method() == Method::GET && req.uri().path().ends_with("/foo") {
        Ok(Response::new(full(HTMLPAGE)))
    } else if req.method() == Method::GET && req.uri().path().ends_with("/joke") {
        use_reqwest(req).await
    } else if req.method() == Method::POST {
        post_echo(req).await
    } else {
            // Return 404 not found response.
            Ok(Response::builder()
                .status(StatusCode::NOT_FOUND)
                .body(full(NOTFOUND))
                .unwrap())
    }
}

fn full<T: Into<Bytes>>(chunk: T) -> BoxBody {
    Full::new(chunk.into())
        .map_err(|never| match never {})
        .boxed()
}

#[tokio::main]
async fn main() -> Result<()> {

    let addr : SocketAddr = "[::]:3000".parse().unwrap();
    let listener = TcpListener::bind(addr).await?;

    loop {
        let (stream, _) = listener.accept().await?;

        let io = TokioIo::new(stream);

        tokio::task::spawn(async move {
            if let Err(err) = http1::Builder::new()
                .serve_connection(io, service_fn(routes))
                .await
            {
                eprintln!("Error serving connection: {:?}", err);
            }
        });
    }
}
